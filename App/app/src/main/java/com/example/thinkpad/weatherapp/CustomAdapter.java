package com.example.thinkpad.weatherapp;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by ThinkPad on 05/05/2016.
 */
public class CustomAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] windArr;
    private final String[] tempArr;
    private final String[] desArr;
    private final String[] dateTimeArr;
    private final Integer[] imgArr;

    public CustomAdapter(Activity context,
                         String[] windArr,
                         String[] tempArr,
                         String[] desArr,
                         String[] dateTimeArr,
                         Integer[] imgArr) {
        super(context, R.layout.activity_listview, windArr);
        this.context = context;
        this.windArr = windArr;
        this.tempArr = tempArr;
        this.desArr = desArr;
        this.dateTimeArr = dateTimeArr;
        this.imgArr = imgArr;
    }

    public View getView(int position,View view,ViewGroup parent) {
        LayoutInflater inflater=context.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.activity_listview, null, true);

        TextView desTV = (TextView) rowView.findViewById(R.id.desTV);
        TextView tempTV = (TextView) rowView.findViewById(R.id.tempTV);
        TextView windTV = (TextView) rowView.findViewById(R.id.windTV);
        TextView dateTimeTV = (TextView) rowView.findViewById(R.id.dateTimeTV);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        desTV.setText(desArr[position]);
        tempTV.setText(tempArr[position] + " °C");
        windTV.setText(windArr[position] + " m/s");
        dateTimeTV.setText(dateTimeArr[position]);
        imageView.setImageResource(imgArr[position]);

        return rowView;

    };
}