package com.example.thinkpad.weatherapp;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    FeedReaderDbHelper mDbHelper = new FeedReaderDbHelper(this);
    String[] desArr = {"Sunny","Cloudy", "Rain"};
    String[] windArr = {"3","7", "24"};
    String[] tempArr = {"-4","6", "20"};
    String[] dateTimeArr = {
            "2014-05-05 kl 5",
            "2014-05-05 kl 10",
            "2014-05-05 kl 30"};
    Integer[] imageId = {
            R.drawable.sun,
            R.drawable.cloud,
            R.drawable.rain,
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CustomAdapter adapter=new CustomAdapter(this, windArr, tempArr, desArr, dateTimeArr, imageId);
        ListView listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setClickable(false);

        deleteDb("1", "entry");
        putDb("1", "hello rain");
        putDb("1", "heleeelo rain");
        putDb("2", "goodbye sun");
        readDb("1");
        readDb("2");
        Log.d("readDb", "hej");

        Button OpenWeatherMapButton = (Button) findViewById(R.id.OpenWeatherMapButton);
        OpenWeatherMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currentWeather();
            }
        });
    }
    private void currentWeather() {
        String url = "http://api.openweathermap.org/data/2.5/weather?q=Aarhus&APPID=382d5441eadc62d861fd0a5abd8e8002&units=metric";
        //The async task can be executed only once, so a new instance is created
        WeatherHandler weatherHandler = new WeatherHandler();
        weatherHandler.execute(url);
        Log.d("currentWeather", "pressed");
    }

    private void putDb(String id, String des){
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_ENTRY_ID, id);
        values.put(FeedReaderContract.FeedEntry.COLUMN_NAME_DES, des);

        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                FeedReaderContract.FeedEntry.TABLE_NAME,
                null,
                values);
    }

    private void readDb(String rowId){
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                FeedReaderContract.FeedEntry._ID,
                FeedReaderContract.FeedEntry.COLUMN_NAME_DES
        };
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(rowId) };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = FeedReaderContract.FeedEntry.COLUMN_NAME_DES + " DESC";

        Cursor c = db.query(
                FeedReaderContract.FeedEntry.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        c.moveToFirst();
        String itemId = c.getString(
                c.getColumnIndexOrThrow(FeedReaderContract.FeedEntry.COLUMN_NAME_DES)
        );
        Log.d("itemID", itemId);
    }
        private void deleteDb(String rowId, String table_name){
        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        // Define 'where' part of query.
        String selection = FeedReaderContract.FeedEntry.COLUMN_NAME_ENTRY_ID + " LIKE ?";
        // Specify arguments in placeholder order.
        String[] selectionArgs = { String.valueOf(rowId) };
        // Issue SQL statement.
        db.delete(table_name, selection, selectionArgs);
    }




    // service
    public void startOnClick(View view) { // todo
        startBackgroundService();
    }

    public void stopOnClick(View view) {
        stopBackgroundService();
    }

    private void startBackgroundService() {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, MyService.class);
        long waitTime = 5*1000;
        backgroundServiceIntent.putExtra("waitTime", waitTime);
        startService(backgroundServiceIntent);
    }

    private void stopBackgroundService() {
        Intent backgroundServiceIntent = new Intent(MainActivity.this, MyService.class);
        stopService(backgroundServiceIntent);
        Log.d("MAIN", "Stopping service!!");
    }

    private void handleBackgroundResult(String result){
        Toast.makeText(MainActivity.this, "Got result from background service:\n" + result, Toast.LENGTH_SHORT).show();
    }

    private BroadcastReceiver onBackgroundServiceResult = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("MAIN", "Broadcast received from bg service");
            String result = intent.getStringExtra("result");
            if(result==null){
                result = "ERROR";
            }
            handleBackgroundResult(result);
        }
    };

    @Override
    protected void onResume() {
        Log.d("Main", "registering receivers");

        IntentFilter filter = new IntentFilter();
        filter.addAction("weatherService");

        //can use registerReceiver(...)
        //but using local broadcasts for this service:
        LocalBroadcastManager.getInstance(this).registerReceiver(onBackgroundServiceResult, filter);
        super.onResume();
    }

    @Override
    protected void onPause() {

        Log.d("main", "unregistering receivers, onpause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onBackgroundServiceResult);
        super.onPause();
    }
}
