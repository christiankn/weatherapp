package com.example.thinkpad.weatherapp;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class WeatherHandler extends AsyncTask<Object,Void,String>  {

    @Override
    protected String doInBackground(Object... params) {
        String response = "No Response";
        String url = (String)params[0];
        response = GETrequest(url);
        return response;
    }

    private String GETrequest(String urlStr) {
        String response = "No GET response";
        try{
            URL url = new URL(urlStr);
            URLConnection conn = url.openConnection();
            Log.d("conn", conn.toString());
            response = getResponse(conn);
        }catch(Exception e){}
        return response;
    }

    //read response and put it together as a String
    public String getResponse(URLConnection conn) {
        String response = "can't read response";
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            StringBuffer sb = new StringBuffer();
            while ((line = in.readLine()) != null) {
                sb.append(line);
            }
            in.close();
            response  = sb.toString();
        }catch(Exception e){e.printStackTrace();}
        Log.d("response", response);
        JsonHandler(response);
        return response;
    }
    private void JsonHandler(String input) {
        try {
            JSONObject jsonObject = new JSONObject(input);

            JSONArray jsonWeatherArr = jsonObject.getJSONArray("weather");
            JSONObject jsonWind = jsonObject.getJSONObject("wind");
            JSONObject jsonTemp = jsonObject.getJSONObject("main");
            JSONObject jsonWeatherDes = jsonWeatherArr.getJSONObject(0);
            String weatherDes = jsonWeatherDes.getString("description");
            String windSpeed = jsonWind.getString("speed");
            String temp = jsonTemp.getString("temp");
            String city = jsonObject.getString("name");

            Log.d("city", city);
            Log.d("temp", temp);
            Log.d("weather description", weatherDes);
            Log.d("windSpeed", windSpeed);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    /*
    DBHandler dhHandler = new DBHandler();
    private void putIntoDB(String id, String temp, String des, String wind){
        // Gets the data repository in write mode
        SQLiteDatabase db = dhHandler.getWritableDatabase();
        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put( WeatherDB.WeatherTable.COLUMN_NAME_ENTRY_ID, id);
        values.put( WeatherDB.WeatherTable.COLUMN_NAME_WIND, wind);
        values.put( WeatherDB.WeatherTable.COLUMN_NAME_TEMP, temp);
        values.put( WeatherDB.WeatherTable.COLUMN_NAME_DES, des);
        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                WeatherDB.WeatherTable.TABLE_NAME,
                WeatherDB.WeatherTable.COLUMN_NAME_NULLABLE,
                values);
    }
    */
}


