package com.example.thinkpad.weatherapp;

import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class MyService extends Service {
    public MyService() {
    }

    private boolean started = false;
    private long wait;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("MyService", "Background service onCreate");
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null; // not used in bg services
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //in this case we only start the background running loop once
        if(!started && intent!=null) {
            wait = intent.getLongExtra("waitTime", 10000);
            Log.d("MyService", "Background service onStartCommand with wait: " + wait + "ms");
            started = true;
            doBackgroundThing(wait);
        }
        return START_STICKY;
    }

    private void doBackgroundThing(final long wait) {

        AsyncTask task = new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {
                String s = "Background job";
                try {
                    Log.d("MyService", "Task started");
                    Thread.sleep(wait);
                    Log.d("MyService", "Task completed");
                } catch (Exception e) {
                    s += " did not finish due to error";
                    //e.printStackTrace();
                    return s;
                }

                s += " completed after " + wait + "ms";
                return s;
            }
            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                broadcastTaskResult((String)o);

                //if Service is still running, keep doing this recursively
                if(started){
                    doBackgroundThing(wait);
                }
            }
        };

        task.execute();

    }

    private void broadcastTaskResult(String result) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("weatherService");
        broadcastIntent.putExtra("result", result);
        Log.d("MyService", "Broadcasting:" + result);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    public void onDestroy() {
        started = false;
        Log.d("MyService","Background service destroyed");
        super.onDestroy();
    }
}
